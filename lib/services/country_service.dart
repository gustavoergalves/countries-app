import 'dart:convert';

import 'package:countries_app/models/country.dart';
import 'package:http/http.dart';

class CountryService {
  List<Country> countries;
  Country country;

  CountryService();

  Future<List<String>> fetchAllCountries() async {
    Response response =
        await get('https://restcountries.eu/rest/v2/all?fields=name');
    if (response.statusCode == 200) {
      return (json.decode(response.body) as List)
          .map((e) => e["name"].toString())
          .toList();
    } else {
      throw Exception('Failed to load countries');
    }
  }

  Future<List<Country>> fetchAllCountriesByRegion(String region) async {
    Response response = await get(
        'https://restcountries.eu/rest/v2/region/$region?fields=alpha2Code;name;flag;capital;region;subregion;population;languages;borders');
    if (response.statusCode == 200) {
      return (json.decode(response.body) as List)
          .map((e) => new Country.fromJson(e))
          .toList();
    } else {
      throw Exception('Failed to load countries');
    }
  }

  Future<List<Country>> fetchAllCountriesByCountryName(
      String countryName) async {
    Response response = await get(
        'https://restcountries.eu/rest/v2/name/$countryName?fullText=true&fields=alpha2Code;name;flag;capital;region;subregion;population;languages;borders');
    if (response.statusCode == 200) {
      return (json.decode(response.body) as List)
          .map((e) => new Country.fromJson(e))
          .toList();
    } else {
      throw Exception('Failed to load countries');
    }
  }

  Future<void> fetchCountryByAlpha(String alpha) async {
    Response response = await get(
        'https://restcountries.eu/rest/v2/alpha/$alpha?fields=alpha2Code;name;flag;capital;region;subregion;population;languages;borders');
    if (response.statusCode == 200) {
      country = new Country.fromJson(json.decode(response.body));
    } else {
      country = new Country('', '', '', '', '', '', '', [], []);
    }
  }

  Future<void> fetchCountriesByBorders(String borders) async {
    Response response =
        await get('https://restcountries.eu/rest/v2/alpha?codes=$borders');
    if (response.statusCode == 200) {
      countries = (json.decode(response.body) as List)
          .map((e) => new Country.fromJson(e))
          .toList();
    } else {
      countries = [];
    }
  }
}
