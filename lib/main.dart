import 'package:countries_app/screens/countries/countries.dart';
import 'package:countries_app/screens/country_details/country_details.dart';
import 'package:flutter/material.dart';

const CountriesRoute = '/';
const CountryDetailsRoute = '/country_details';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(onGenerateRoute: routes());
  }
}

RouteFactory routes() {
  return (settings) {
    final Map<String, dynamic> arguments = settings.arguments;
    Widget screen;
    switch (settings.name) {
      case CountriesRoute:
        screen = Countries();
        break;
      case CountryDetailsRoute:
        screen = CountryDetails(alpha2Code: arguments['alpha2Code']);
        break;
      default:
        return null;
    }
    return MaterialPageRoute(builder: (BuildContext context) => screen);
  };
}
