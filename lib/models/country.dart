import 'language.dart';

class Country {
  final String alpha2Code;
  final String name;
  final String flag;
  final String capital;
  final String region;
  final String subregion;
  final String population;
  final List<Language> languages;
  final List<String> borders;

  Country(this.alpha2Code, this.name, this.flag, this.capital, this.region,
      this.subregion, this.population, this.languages, this.borders);

  factory Country.fromJson(Map<String, dynamic> jsonData) {
    return Country(
      jsonData['alpha2Code'],
      jsonData['name'],
      jsonData['flag'],
      jsonData['capital'],
      jsonData['region'],
      jsonData['subregion'],
      jsonData['population'].toString(),
      (jsonData['languages'] as List)
          .map((e) => new Language.fromJson(e))
          .toList(),
      (jsonData['borders'] as List).map((e) => e.toString()).toList(),
    );
  }
}
