class Language {
  final String name;
  final String nativeName;

  Language(this.name, this.nativeName);

  factory Language.fromJson(Map<String, dynamic> jsonData) {
    return Language(
      jsonData['name'],
      jsonData['nativeName'],
    );
  }
}
