import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ImageBanner extends StatelessWidget {
  final String path;

  ImageBanner(this.path);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.fromLTRB(30, 30, 30, 0),
        constraints: BoxConstraints.expand(height: 200.0),
        child: new SvgPicture.network(path));
  }
}
