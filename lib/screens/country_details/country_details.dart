import 'package:countries_app/models/country.dart';
import 'package:countries_app/screens/countries/flag_list.dart';
import 'package:countries_app/screens/country_details/text_section.dart';
import 'package:countries_app/services/country_service.dart';
import 'package:flutter/material.dart';

import '../../main.dart';
import 'image_banner.dart';

class CountryDetails extends StatefulWidget {
  final String alpha2Code;

  const CountryDetails({Key key, this.alpha2Code}) : super(key: key);

  @override
  CountryDetailsState createState() => CountryDetailsState();
}

class CountryDetailsState extends State<CountryDetails> {
  Country country = new Country('', '', '', '', '', '', '', [], []);
  List<Country> countriesBorders = [];

  fetchCountryByAlpha(alpha) async {
    CountryService instance = CountryService();
    await instance.fetchCountryByAlpha(alpha);
    await instance.fetchCountriesByBorders(instance.country.borders.join(';'));
    setState(() {
      this.country = instance.country;
      this.countriesBorders = instance.countries;
    });
  }

  @override
  void initState() {
    super.initState();
    fetchCountryByAlpha(widget.alpha2Code);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(70.0),
        child: AppBar(
          actions: [
            Container(
              margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                child: RaisedButton(
              onPressed: () =>
                  Navigator.pushReplacementNamed(context, CountriesRoute),
              child: Text('Voltar'),
              textColor: Colors.white,
              color: Colors.cyan[600],
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(18.0),
              ),
            ))
          ],
          backgroundColor: Colors.grey[200],
          title: Container(
            child: Image.network(
                "https://cursolibras.incluirtecnologia.com.br/incluir_logo.png"),
            padding: EdgeInsets.only(top: 5),
          ),
        ),
      ),
      body: Column(
        children: [
          ImageBanner(country.flag),
          TextSection(country.name, country.capital, country.region,
              country.subregion, country.population, country.languages.map((e) => e.nativeName).join(', ')),
          Text('Países Vizinhos', textAlign: TextAlign.center),
          FlagList(countriesBorders)
        ],
      ),
    );
  }
}
