import 'package:flutter/material.dart';

class TextSection extends StatelessWidget {
  final String name;
  final String capital;
  final String region;
  final String subregion;
  final String population;
  final String languages;

  static const double hPad = 16.0;

  TextSection(
      this.name, this.capital, this.region, this.subregion, this.population, this.languages);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
            padding: const EdgeInsets.fromLTRB(hPad, 32.0, hPad, 4.0),
            child: Text('Nome: ' + name)),
        Container(
            padding: const EdgeInsets.fromLTRB(hPad, 10.0, hPad, 4.0),
            child: Text('Capital: ' + capital)),
        Container(
            padding: const EdgeInsets.fromLTRB(hPad, 10.0, hPad, hPad),
            child: Text('Região: ' + region)),
        Container(
            padding: const EdgeInsets.fromLTRB(hPad, 10.0, hPad, hPad),
            child: Text('Sub Região: ' + subregion)),
        Container(
            padding: const EdgeInsets.fromLTRB(hPad, 10.0, hPad, hPad),
            child: Text('População: ' + population)),
        Container(
            padding: const EdgeInsets.fromLTRB(hPad, 10.0, hPad, hPad),
            child: Text('Línguas: ' + languages)),
      ],
    );
  }
}
