import 'package:countries_app/models/country.dart';
import 'package:countries_app/screens/countries/flag_icon.dart';
import 'package:flutter/material.dart';

import '../../main.dart';

class FlagList extends StatelessWidget {
  final List<Country> countries;

  FlagList(this.countries);

  @override
  Widget build(BuildContext context) {
    return new Expanded(
        child: ListView(
            children: countries
                .map((country) => GestureDetector(
                      child: FlagIcon(country.flag),
                      onTap: () => Navigator.pushReplacementNamed(
                          context, CountryDetailsRoute,
                          arguments: {'alpha2Code': country.alpha2Code}),
                    ))
                .toList()));
  }
}
