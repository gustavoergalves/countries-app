import 'package:countries_app/models/country.dart';
import 'package:countries_app/services/country_service.dart';
import 'package:flutter/material.dart';

import 'flag_list.dart';

class Countries extends StatefulWidget {
  @override
  CountriesState createState() => CountriesState();
}

class CountriesState extends State<Countries> {
  Future<List<Country>> countries;
  Future<List<String>> countriesSelect;
  String regionSelected;
  String countrySelected;

  fetchData() async {
    CountryService instance = CountryService();
    if (countrySelected != null) {
      setState(() {
        countries = instance.fetchAllCountriesByCountryName(countrySelected);
      });
    } else if (regionSelected != null) {
      setState(() {
        countries = instance.fetchAllCountriesByRegion(regionSelected);
      });
    }
  }

  fetchSelectData() async {
    CountryService instance = CountryService();
    countriesSelect = instance.fetchAllCountries();
  }

  @override
  void initState() {
    super.initState();
    fetchSelectData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child: AppBar(
            backgroundColor: Colors.grey[200],
            title: Container(
              child: Image.network(
                  "https://cursolibras.incluirtecnologia.com.br/incluir_logo.png"),
              padding: EdgeInsets.only(top: 5),
            ),
          ),
        ),
        body: Column(
          children: [
            Container(
                padding: EdgeInsets.all(30),
                child: DropdownButton<String>(
                  isExpanded: true,
                  hint: new Text("Região", textAlign: TextAlign.center),
                  value: regionSelected,
                  items: ['Africa', 'Americas', 'Asia', 'Europe', 'Oceania']
                      .map((label) => DropdownMenuItem(
                            child: Text(label),
                            value: label,
                          ))
                      .toList(),
                  onChanged: (value) {
                    setState(() {
                      regionSelected = value;
                      countrySelected = null;
                    });
                  },
                )),
            Container(
                padding: EdgeInsets.all(30),
                child: FutureBuilder<List<String>>(
                    future: countriesSelect,
                    builder: (context, snapshot) {
                      List<String> data = [];
                      if (snapshot.hasData) {
                        data = snapshot.data;
                      }
                      return DropdownButton<String>(
                        isExpanded: true,
                        hint: new Text("País", textAlign: TextAlign.center),
                        value: countrySelected,
                        items: data
                            .map((label) => DropdownMenuItem(
                                  child: Text(label),
                                  value: label,
                                ))
                            .toList(),
                        onChanged: (value) {
                          setState(() {
                            countrySelected = value;
                            regionSelected = null;
                          });
                        },
                      );
                    })),
            RaisedButton(
              onPressed: () => fetchData(),
              child: Text('Pesquisar'),
              textColor: Colors.white,
              color: Colors.cyan[600],
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(18.0),
              ),
            ),
            FutureBuilder<List<Country>>(
                future: countries,
                builder: (context, snapshot) {
                  List<Country> data = [];
                  if (snapshot.hasData) {
                    data = snapshot.data;
                  }
                  return FlagList(data);
                })
          ],
        ));
  }
}
