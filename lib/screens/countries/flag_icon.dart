import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FlagIcon extends StatelessWidget {
  final String path;

  FlagIcon(this.path);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10),
        child: new SvgPicture.network(path, width: 150, height: 150));
  }
}
